@echo off
echo Bootstrapping the buildout, please wait...
if exist c:\Python35\python.exe (
  c:\Python35\python.exe bootstrap.py
) else (
  if exist c:\Python34\python.exe (
    c:\Python34\python.exe bootstrap.py
  ) else (
    c:\Python33\python.exe bootstrap.py
  )
)
pause
