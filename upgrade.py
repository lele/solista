# -*- coding: utf-8 -*-
# :Progetto:  SoLista -- Upgrade configuration files
# :Creato:    sab 22 mar 2014 17:12:50 CET
# :Autore:    Lele Gaifax <lele@metapensiero.it>
# :Licenza:   GNU General Public License version 3 or later
#

from datetime import datetime
from io import BytesIO
from os import makedirs, rename
from os.path import dirname, exists, splitext, normpath
from urllib.request import urlopen
from zipfile import ZipFile

url = "https://bitbucket.org/lele/solista/get/master.zip"
now = datetime.now().replace(microsecond=0)

def rename_old(pathname):
    name, ext = splitext(pathname)
    newpathname = "%s-%s%s" % (name, now.isoformat().replace(':', '_'), ext)
    print("Backing up %s as %s..." % (pathname, newpathname))
    rename(pathname, newpathname)

def main():
    try:
        print('Fetching %s...' % url)
        f = urlopen(url)
    except IOError:
        print('Could not fetch SoLista archive from %s!' % (url,))
    else:
        didsomething = False

        print('Extracting zip content...')

        z = ZipFile(BytesIO(f.read()))
        paths = z.namelist()
        for path in sorted(paths):
            if path.endswith('/'):
                continue
            subpath = normpath(path.split('/', 1)[-1])
            subdir = dirname(subpath)
            if subdir and not exists(subdir):
                makedirs(subdir)
            newcontent = z.read(path)
            if exists(subpath):
                with open(subpath, 'rb') as f:
                    oldcontent = f.read()
                if newcontent == oldcontent:
                    continue
                rename_old(subpath)
            didsomething = True
            print("Updating %s..." % subpath)
            with open(subpath, 'wb') as f:
                f.write(newcontent)

        if didsomething:
            print("Done, you should re-execute the buildout now...")
        else:
            print("Nothing done, the configuration is already up-to-date!")

if __name__ == '__main__':
    main()
