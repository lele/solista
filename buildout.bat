@echo off
echo Executing the buildout, it will take a couple of minutes, please wait...
bin\buildout.exe 2>&1 | bin\wtee -a buildout.log
pause
